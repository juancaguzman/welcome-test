import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    listSelected: [false, false, false, false, false]
  },
  getters: {
    isStepSelected: state => index => state.listSelected[index]
  },
  mutations: {
    ["LISTSELECTED_CLEAN"](state) {
      state.listSelected = [false, false, false, false, false];
    },
    ["LISTSELECTED_SELECT"](state, payload) {
      state.listSelected[payload] = true;
    }
  },
  actions: {
    selectStep({ commit }, payload) {
      commit("LISTSELECTED_CLEAN");
      commit("LISTSELECTED_SELECT", payload);
    },
    cleanSteps({ commit }) {
      commit("LISTSELECTED_CLEAN");
    }
  }
});
