import Vue from "vue";
import Router from "vue-router";
import Login from "./views/Login.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "login",
      components: {
        primary: Login
      }
    },
    {
      path: "/register",
      name: "register",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        primary: () =>
          import(/* webpackChunkName: "about" */ "./views/Register.vue")
      }
    },
    {
      path: "/list",
      name: "list",
      components: {
        primary: () => import("./views/ContentPage.vue")
      },
      children: [
        {
          path: "/type",
          name: "list-type",
          components: {
            secundary: () => import("./components/ListType.vue")
          }
        },
        {
          path: "/creation",
          name: "list-creation",
          components: {
            secundary: () => import("./components/ListCreation.vue")
          }
        }
      ]
    },
    {
      path: "/start",
      // name: "start",
      redirect: "entry",
      components: {
        primary: () => import("./views/ContentPage.vue")
      },
      children: [
        {
          path: "/",
          name: "entry",
          components: {
            // primary: () => import("./views/ContentPage.vue"),
            secundary: () => import("./components/EntryForm.vue")
          }
        }
      ]
    },
    {
      path: "/prueba",
      name: "primary",
      components: {
        primary: () => import("./components/HeroComponent.vue")
      }
    }
  ]
});
