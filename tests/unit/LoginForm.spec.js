import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import LoginForm from "@/components/LoginForm.vue";

describe("LoginForm.vue", () => {
  const wrapper = shallowMount(LoginForm);
  it("renders all text in the form", () => {
    expect(wrapper.text()).to.include("Iniciar Sesion");
  });
  it("Tiene un formulario", () => {
    expect(wrapper.find("form")).to.exist;
  });
});
